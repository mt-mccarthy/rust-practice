// An in-place merge sort implementation.
// arr: The array to be sorted **the input will be modified**
pub fn merge_sort(arr: &mut [i32]) {
    let l = arr.len();
    if l > 1 {
        let mut iter1 = 0; // Marks start of unmerged sub-lists
        let mut iter2 = l / 2; // Marks the start of the second half of the list

        merge_sort(&mut arr[0..iter2]);
        merge_sort(&mut arr[iter2..l]);

        // Assumptions: arr[0 .. iter2] and arr[iter2 .. l] are sorted smallest to largest
        // Invariants:  arr[0 .. iter1], arr[iter1 .. iter2], arr[iter2 .. l] are all sorted smallest to largest
        //              arr[iter1 - 1] < arr[iter1] when iter1 >= 1
        //              arr[iter1 - 1] < arr[iter2] when iter1 >= 1 and iter2 < l
        // Convention:  arr[0 .. iter1] represents elements have already been "merged" into arr;
        //              arr[iter1 .. iter2] and arr[iter2 .. l] are the sublists that haven't yet been merged

        while iter1 < iter2 && iter2 < l {
            // If the first entry of the left half is no larger than the first entry of the right half
            // the correct element lies in the iter1 position of the array
            if arr[iter1] <= arr[iter2] {
                iter1 = iter1 + 1;
            }
            // Otherwise, we cyclically shift arr[iter1 .. iter2+1] up one index
            else {
                let mut cur;
                let mut old = arr[iter1];
                let mut k = iter1;
                while k < iter2 {
                    cur = arr[k + 1];
                    arr[k + 1] = old;
                    old = cur;
                    k = k + 1;
                }
                arr[iter1] = old;

                iter1 = iter1 + 1;
                iter2 = iter2 + 1;
            }
        }
    }
}

fn sift_down(arr: &mut [i32], start: usize) {}

fn heapify(arr: &mut [i32]) {
    let l = arr.len();

    let mut start = l - 1;

    while start >= 0 {
        sift_down(arr, start);
        start = start - 1;
    }
}

pub fn heap_sort(arr: &mut [i32]) {}
