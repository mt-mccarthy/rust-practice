mod sort_algos;

fn main() {
    println!("Hello, world!");
}

#[cfg(test)]
mod merge_sort_tests {
    use super::*;

    #[test]
    fn test1() {
        let mut input = [1, 3, -1, 5, -9, 3];
        let expected = [-9, -1, 1, 3, 3, 5];
        sort_algos::merge_sort(&mut input);
        assert_eq!(input.len(), expected.len());

        let mut i = 0;

        while i < input.len() {
            let a = &input[i];
            let b = &input[i];
            assert_eq!(a, b);
            i = i + 1;
        }
    }
}
