#[cfg(test)]
mod merge_sort_tests {
    use super::*;

    #[test]
    fn test1() {
        input = [1, 3, -1, 5, -9, 3];
        expected = [-9, -1, 1, 3, 3, 5];
        merge_sort(input);
        assert_eq!(input.iter(), expected.iter());
    }
}
